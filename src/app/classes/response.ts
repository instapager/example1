import { Post } from './post';

export interface Response {
    posts: Post[],
    profile_pic: string,
    userid: number,
    username: string
}
