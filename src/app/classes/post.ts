import { IGLocation } from './ig-location';

export interface Post {
    desc?: string,
    image: string,
    likes: number,
    link: string,
    location?: IGLocation,
    thumb: string,
    timestamp: Date
}
