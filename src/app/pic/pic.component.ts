import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../classes/post';
import { faHeart } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-pic',
  templateUrl: './pic.component.html',
  styleUrls: ['./pic.component.less']
})
export class PicComponent implements OnInit {
  faHeart = faHeart;

  @Input() post: Post;

  constructor() { }

  ngOnInit() {

  }

  onClick() {
    window.open(this.post.link);
  }

}
