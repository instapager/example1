import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  public data: Response;
  public loading: boolean = true;

  constructor(
    private http: HttpClient
  ) {}

  setFavicon(url: string): void {
    var favicon = document.querySelector('link[rel="icon"]');
    favicon.setAttribute('type', 'image/png');
    favicon.setAttribute('href', url);
  }

  setTitle(title: string): void {
    document.title = title;
  }

  ngOnInit(): void {
    this.http.get('http://localhost:8081/').subscribe((data: Response) => {
      this.data = data;
      this.setFavicon(data['profile_pic']);
      this.setTitle(data['username']);
      setTimeout(() => {
        this.loading = false;
      }, 500);
    })
  }
}
